package com.todo.todo.domain.repositories.specifications;

import com.todo.todo.domain.models.Task;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class TaskSpecifications {

    private TaskSpecifications() {
    }

    public static Specification<Task> fromPeriod(LocalDate period) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("period"), period);
    }

    public static Specification<Task> withTitleContaining(String title) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("title"), "%"+title+"%");
    }

    public static Specification<Task> isComplete() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isTrue(root.get("complete"));
    }

    public static Specification<Task> isIncomplete() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isFalse(root.get("complete"));
    }
}
