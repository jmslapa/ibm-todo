package com.todo.todo.domain.services;

import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.FilterTasksRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;
import com.todo.todo.domain.models.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ITaskService {
    List<Task> list();
    List<Task> list(FilterTasksRequestModel filterTasksRequestModel);
    Page<Task> list(Pageable pageable);
    Page<Task> list(FilterTasksRequestModel filterTasksRequestModel, Pageable pageable);
    Task findById(Long id);
    Task create(CreateTaskRequestModel createTaskRequestModel);
    void replace(Long id, ReplaceTaskRequestModel replaceTaskRequestModel);
    void delete(Long id);
}
