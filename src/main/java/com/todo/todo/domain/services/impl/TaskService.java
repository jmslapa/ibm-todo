package com.todo.todo.domain.services.impl;

import com.todo.todo.application.exceptions.TaskNotFoundException;
import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.FilterTasksRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;
import com.todo.todo.application.mappers.TaskMapper;
import com.todo.todo.domain.models.Task;
import com.todo.todo.domain.repositories.TaskRepository;
import com.todo.todo.domain.services.ITaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService implements ITaskService {
    private final TaskRepository taskRepository;
    private static final TaskMapper taskMapper = TaskMapper.INSTANCE;

    @Override
    public List<Task> list() {
        return this.taskRepository.findAll();
    }

    @Override
    public List<Task> list(FilterTasksRequestModel filterTasksRequestModel) {
        return this.taskRepository.findAll(filterTasksRequestModel.toSpecification());
    }

    @Override
    public Page<Task> list(Pageable pageable) {
        return this.taskRepository.findAll(pageable);
    }

    @Override
    public Page<Task> list(FilterTasksRequestModel filterTasksRequestModel, Pageable pageable) {
        return this.taskRepository.findAll(filterTasksRequestModel.toSpecification(), pageable);
    }

    @Override
    public Task findById(Long id) {
        return this.taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task create(CreateTaskRequestModel createTaskRequestModel) {
        return this.taskRepository.save(TaskService.taskMapper.toTask(createTaskRequestModel));
    }

    @Override
    public void replace(Long id, ReplaceTaskRequestModel replaceTaskRequestModel) {
        Task toBeReplaced = TaskService.taskMapper.toTask(replaceTaskRequestModel);
        toBeReplaced.setId(this.findById(id).getId());
        this.taskRepository.save(toBeReplaced);
    }

    @Override
    public void delete(Long id) {
        this.taskRepository.delete(this.findById(id));
    }
}
