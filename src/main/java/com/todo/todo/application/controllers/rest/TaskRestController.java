package com.todo.todo.application.controllers.rest;

import com.todo.todo.domain.models.Task;
import com.todo.todo.domain.services.ITaskService;
import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.FilterTasksRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "todos")
@RequiredArgsConstructor
public class TaskRestController {
    private final ITaskService taskService;

    @PostMapping(path = "list")
    public ResponseEntity<Page<Task>> index(
            @RequestBody(required = false) @Valid FilterTasksRequestModel requestModel,
            Pageable pageable
    ) {

        return ResponseEntity.ok().body(
                requestModel != null
                        ? this.taskService.list(requestModel, pageable)
                        : this.taskService.list(pageable)
        );
    }

    @PostMapping(path = "list/all")
    public ResponseEntity<List<Task>> index(@RequestBody(required = false) @Valid FilterTasksRequestModel requestModel) {
        return ResponseEntity.ok().body(
                requestModel != null
                        ? this.taskService.list(requestModel)
                        : this.taskService.list()
        );
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Task> show(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.taskService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Task> store(@RequestBody @Valid CreateTaskRequestModel requestModel) {
        return new ResponseEntity<>(this.taskService.create(requestModel), HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Task> update(@PathVariable Long id, @RequestBody @Valid ReplaceTaskRequestModel requestModel) {
        this.taskService.replace(id, requestModel);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Task> destroy(@PathVariable Long id) {
        this.taskService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
