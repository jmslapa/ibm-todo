package com.todo.todo.application.mappers;

import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;
import com.todo.todo.domain.models.Task;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    Task toTask(CreateTaskRequestModel createTaskRequestModel);
    Task toTask(ReplaceTaskRequestModel replaceTaskRequestModel);
    CreateTaskRequestModel toCreateTaskRequestModel(Task task);
    ReplaceTaskRequestModel toReplaceTaskRequestModel(Task task);
}
