package com.todo.todo.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class TaskNotFoundException extends HttpStatusCodeException {
    public TaskNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Task not found.");
    }
}
