package com.todo.todo.ui.request.models;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class CreateTaskRequestModel {
    @NotNull
    @Size(min = 3, max = 100, message = "The title must have between 3 and 255 characters long.")
    private String title;

    @Size(min = 20, max = 500, message = "The description must have between 20 and 500 characters long.")
    private String description;

    @NotNull
    @FutureOrPresent
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate period;

    @NotNull
    private Boolean complete;
}
