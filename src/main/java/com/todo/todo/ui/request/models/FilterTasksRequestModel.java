package com.todo.todo.ui.request.models;

import com.todo.todo.domain.models.Task;
import com.todo.todo.domain.repositories.specifications.TaskSpecifications;
import lombok.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilterTasksRequestModel {
    @Size(max = 100)
    private String title;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate period;

    private Boolean complete;

    public Specification<Task> toSpecification() {
        Specification<Task> specification = null;
        if(this.title != null) {
            specification = TaskSpecifications.withTitleContaining(this.title);
        }
        if(this.period != null) {
            Specification<Task> fromPeriod = TaskSpecifications.fromPeriod(this.period);
            specification = specification != null ? specification.and(fromPeriod) : fromPeriod;
        }
        if(this.complete != null) {
            Specification<Task> completeAttributeSpecification = Boolean.TRUE.equals(this.complete)
                    ? TaskSpecifications.isComplete()
                    : TaskSpecifications.isIncomplete();
            specification = specification != null
                    ? specification.and(completeAttributeSpecification)
                    : completeAttributeSpecification;
        }
        return specification;
    }
}
