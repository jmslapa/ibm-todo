package com.todo.todo.domain.services.impl;

import com.todo.todo.application.exceptions.TaskNotFoundException;
import com.todo.todo.domain.models.Task;
import com.todo.todo.domain.repositories.TaskRepository;
import com.todo.todo.domain.todo.util.TaskDummySupplier;
import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.FilterTasksRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
class TaskServiceTest {
    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskRepository taskRepositoryMock;

    @BeforeEach
    void setUp() {
        List<Task> taskList = new ArrayList<>(List.of(TaskDummySupplier.makeValidTask()));
        BDDMockito.when(this.taskRepositoryMock.findAll()).thenReturn(taskList);
        BDDMockito.when(this.taskRepositoryMock.findAll(ArgumentMatchers.<Specification<Task>>any()))
                .thenReturn(taskList);

        PageImpl<Task> taskPage = new PageImpl<>(List.of(TaskDummySupplier.makeValidTask()));
        BDDMockito.when(this.taskRepositoryMock.findAll(ArgumentMatchers.any(Pageable.class))).thenReturn(taskPage);
        BDDMockito.when(this.taskRepositoryMock.findAll(
                ArgumentMatchers.<Specification<Task>>any(), ArgumentMatchers.any(Pageable.class)
        )).thenReturn(taskPage);

        Task task = TaskDummySupplier.makeValidTask();
        BDDMockito.when(this.taskRepositoryMock.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(task));

        BDDMockito.when(this.taskRepositoryMock.save(ArgumentMatchers.any(Task.class))).thenReturn(task);

        BDDMockito.doNothing().when(this.taskRepositoryMock).delete(ArgumentMatchers.any(Task.class));
    }

    @Test
    void shouldListReturnAListContainingAtLeastOneTaskWhenSuccessful() {
        Task persisted = TaskDummySupplier.makeValidTask();
        List<Task> tasks = this.taskService.list();
        Assertions.assertThat(tasks).isNotNull().isNotEmpty().contains(persisted);
    }

    @Test
    void shouldListReturnAListContainingAtLeastOneTaskWhenReceiveFiltersAndSuccessful() {
        Task persisted = TaskDummySupplier.makeValidTask();
        FilterTasksRequestModel filters = TaskDummySupplier.makeValidFilterTaskRequestModel();
        List<Task> tasks = this.taskService.list(filters);
        Assertions.assertThat(tasks).isNotNull().isNotEmpty().contains(persisted);
    }

    @Test
    void shouldListReturnAPageContainingAtLeastOneTaskWhenReceivePageableAndSuccessful()
    {
        Task persisted = TaskDummySupplier.makeValidTask();
        Page<Task> tasks = this.taskService.list(BDDMockito.mock(Pageable.class));
        Assertions.assertThat(tasks).isNotNull().isNotEmpty().contains(persisted);
    }

    @Test
    void shouldListReturnAPageContainingAtLeastOneTaskWhenReceiveFiltersAndPageableAndSuccessful()
    {
        Task persisted = TaskDummySupplier.makeValidTask();
        FilterTasksRequestModel filters = TaskDummySupplier.makeValidFilterTaskRequestModel();
        Page<Task> tasks = this.taskService.list(filters, BDDMockito.mock(Pageable.class));
        Assertions.assertThat(tasks).isNotNull().isNotEmpty().contains(persisted);
    }

    @Test
    void shouldFindByIdReturnASingleTaskWhenSuccessful() {
        Task persisted = TaskDummySupplier.makeValidTask();
        Task task = this.taskService.findById(1L);
        Assertions.assertThat(task).isNotNull().isEqualTo(persisted);
    }

    @Test
    void shouldFindByIdThrowATaskNotFoundExceptionWhenSuccessful()
    {
        BDDMockito.when(this.taskRepositoryMock.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.empty());
        Assertions.assertThatThrownBy(() -> this.taskService.findById(1L)).isInstanceOf(TaskNotFoundException.class);
    }

    @Test
    void shouldCreateReturnASingleTaskWhenSuccessful() {
        Task persisted = TaskDummySupplier.makeValidTask();
        CreateTaskRequestModel requestModel = TaskDummySupplier.makeValidCreateTaskRequestModel();
        Task task = this.taskService.create(requestModel);
        Assertions.assertThat(task).isNotNull().isEqualTo(persisted);
    }

    @Test
    void shouldReplaceInvokeTheFindByIdMethodOfTheOwnClassAndTheSaveMethodOfTheTaskRepositoryWhenSuccessful() {
        ReplaceTaskRequestModel requestModel = TaskDummySupplier.makeValidReplaceTaskRequestModel();
        Assertions.assertThatCode(() -> this.taskService.replace(1L, requestModel)).doesNotThrowAnyException();
        Mockito.verify(this.taskRepositoryMock, Mockito.times(1))
                .save(ArgumentMatchers.any(Task.class));
    }

    @Test
    void shouldReplaceThrowATaskNotFoundExceptionWhenSuccessful() {
        ReplaceTaskRequestModel requestModel = TaskDummySupplier.makeValidReplaceTaskRequestModel();
        BDDMockito.when(this.taskRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.empty());
        Assertions.assertThatThrownBy(() -> this.taskService.replace(1L, requestModel))
                .isInstanceOf(TaskNotFoundException.class);
    }

    @Test
    void shouldDeleteInvokeTheDeleteMethodOfTheTaskRepositoryWhenSuccessful() {
        Assertions.assertThatCode(() -> this.taskService.delete(1L)).doesNotThrowAnyException();
        Mockito.verify(this.taskRepositoryMock, Mockito.times(1))
                .delete(ArgumentMatchers.any(Task.class));
    }

    @Test
    void shouldDeleteThrowATaskNotFoundExceptionWhenSuccessful() {
        BDDMockito.when(this.taskRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.empty());
        Assertions.assertThatThrownBy(() -> this.taskService.delete(1L)).isInstanceOf(TaskNotFoundException.class);
    }
}