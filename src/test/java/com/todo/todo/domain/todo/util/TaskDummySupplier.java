package com.todo.todo.domain.todo.util;

import com.todo.todo.application.mappers.TaskMapper;
import com.todo.todo.domain.models.Task;
import com.todo.todo.ui.request.models.CreateTaskRequestModel;
import com.todo.todo.ui.request.models.FilterTasksRequestModel;
import com.todo.todo.ui.request.models.ReplaceTaskRequestModel;

import java.time.LocalDate;

public class TaskDummySupplier {
    private static final TaskMapper taskMapper = TaskMapper.INSTANCE;

    public static Task makeValidTask() {
        return Task.builder()
                .title("Dummy title")
                .description("Dummy description with at least 20 characters.")
                .period(LocalDate.now())
                .complete(false)
                .build();
    }

    public static FilterTasksRequestModel makeValidFilterTaskRequestModel() {
        Task task = TaskDummySupplier.makeValidTask();
        return FilterTasksRequestModel.builder()
                .title(task.getTitle())
                .period(task.getPeriod())
                .complete(task.getComplete())
                .build();
    }

    public static CreateTaskRequestModel makeValidCreateTaskRequestModel() {
        return TaskDummySupplier.taskMapper.toCreateTaskRequestModel(TaskDummySupplier.makeValidTask());
    }

    public static ReplaceTaskRequestModel makeValidReplaceTaskRequestModel() {
        return TaskDummySupplier.taskMapper.toReplaceTaskRequestModel(TaskDummySupplier.makeValidTask());
    }
}
