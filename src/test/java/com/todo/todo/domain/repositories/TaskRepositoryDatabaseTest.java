package com.todo.todo.domain.repositories;

import com.todo.todo.domain.models.Task;
import com.todo.todo.domain.repositories.specifications.TaskSpecifications;
import com.todo.todo.domain.todo.util.TaskDummySupplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@DataJpaTest
class TaskRepositoryDatabaseTest {
    @Autowired
    private TaskRepository taskRepository;

    @Test
    void shouldSavePersistAValidTaskWhenSuccessful() {
        Task toBePersisted = TaskDummySupplier.makeValidTask();
        Task persisted = this.taskRepository.save(toBePersisted);
        toBePersisted.setId(persisted.getId());

        Assertions.assertThat(persisted).isNotNull().isEqualTo(toBePersisted);
        Assertions.assertThat(persisted.getId()).isNotNull();
    }

    @Test
    void shouldSaveUpdateAnExistingTaskWhenSuccessful() {
        Task toBePersisted = TaskDummySupplier.makeValidTask();
        Task toBeUpdated = this.taskRepository.save(toBePersisted);
        toBeUpdated.setTitle("New title");
        Task updated = this.taskRepository.save(toBeUpdated);

        Assertions.assertThat(updated).isNotNull().isEqualTo(toBeUpdated);
    }

    @Test
    void shouldFindByIdRetrieveAnExistingTaskWhenSuccessful() {
        Task toBeRetrieved = this.taskRepository.save(TaskDummySupplier.makeValidTask());
        Optional<Task> task = this.taskRepository.findById(toBeRetrieved.getId());

        Assertions.assertThat(task).isNotEmpty();
        Assertions.assertThat(task.orElse(null)).isEqualTo(toBeRetrieved);
    }

    @Test
    void findByIdShouldNotRetrieveAnyTasksWhenSuccessful() {
        Optional<Task> task = this.taskRepository.findById(1L);

        Assertions.assertThat(task).isEmpty();
    }

    @Test
    void shouldDeleteExcludeAnExistingTaskWhenSuccessful() {
        Task toBeDeleted = this.taskRepository.save(TaskDummySupplier.makeValidTask());
        this.taskRepository.delete(toBeDeleted);
        Optional<Task> task = this.taskRepository.findById(toBeDeleted.getId());

        Assertions.assertThat(task).isEmpty();
    }

    @Test
    void shouldFindAllRetrieveAListWithAtLeastOneTaskWhenReceiveTheSpecificationWithTitleContainingAndSuccessful() {
        Task toBeRetrieved = this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.withTitleContaining(toBeRetrieved.getTitle()));

        Assertions.assertThat(tasks).isNotEmpty().contains(toBeRetrieved);
    }

    @Test
    void shouldFindAllRetrieveAnEmptyListWhenReceiveTheSpecificationWithTitleContainingAndSuccessful()
    {
        this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.withTitleContaining("Any title"));

        Assertions.assertThat(tasks).isEmpty();
    }

    @Test
    void shouldFindAllRetrieveAListWithAtLeastOneTaskWhenReceiveTheSpecificationFromPeriodAndSuccessful() {
        Task toBeRetrieved = this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.fromPeriod(toBeRetrieved.getPeriod()));

        Assertions.assertThat(tasks).isNotEmpty().contains(toBeRetrieved);
    }

    @Test
    void shouldFindAllRetrieveAnEmptyListWhenReceiveTheSpecificationFromPeriodAndSuccessful() {

        this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.fromPeriod(LocalDate.now().minusDays(1)));

        Assertions.assertThat(tasks).isEmpty();
    }

    @Test
    void shouldFindAllRetrieveAListWithAtLeastOneTaskWhenReceiveTheSpecificationIsCompleteAndSuccessful()
    {
        Task toBePersisted = TaskDummySupplier.makeValidTask();
        toBePersisted.setComplete(true);
        Task toBeRetrieved = this.taskRepository.save(toBePersisted);
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.isComplete());

        Assertions.assertThat(tasks).isNotEmpty().contains(toBeRetrieved);
    }

    @Test
    void shouldFindAllRetrieveAnEmptyListWhenReceiveTheSpecificationIsCompleteAndSuccessful()
    {
        this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.isComplete());
        Assertions.assertThat(tasks).isEmpty();
    }

    @Test
    void shouldFindAllRetrieveAListWithAtLeastOneTaskWhenReceiveTheSpecificationIsIncompleteAndSuccessful()
    {
        Task toBeRetrieved = this.taskRepository.save(TaskDummySupplier.makeValidTask());
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.isIncomplete());

        Assertions.assertThat(tasks).isNotEmpty().contains(toBeRetrieved);
    }

    @Test
    void shouldFindAllRetrieveAnEmptyListWhenReceiveTheSpecificationIsIncompleteAndSuccessful()
    {
        Task toBePersisted = TaskDummySupplier.makeValidTask();
        toBePersisted.setComplete(true);
        this.taskRepository.save(toBePersisted);
        List<Task> tasks = this.taskRepository.findAll(TaskSpecifications.isIncomplete());
        Assertions.assertThat(tasks).isEmpty();
    }
}